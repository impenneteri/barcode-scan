package com.junelsoft.barcodescan;

import android.app.Activity;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.IBinder;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.generalscan.OnConnectedListener;
import com.generalscan.OnDisconnectListener;
import com.generalscan.SendConstant;
import com.generalscan.bluetooth.BluetoothConnect;
import com.generalscan.bluetooth.BluetoothSettings;
//import com.junelb.automatiq.core.MSSQLDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MainActivity extends AppCompatActivity {
    private Activity myActivity;
    private ReadBroadcast mReadBroadcast;

//    private static final MSSQLDatabase mMSSQLDatabase = new MSSQLDatabase();
    private String mMSSQLConnectionString;

    // Controls
    Button btnSelectDevice;
    Button btnConnect;
    Button btnDisconnect;
    TextView tvGetData;
    private ScrollView scrollViewInventoryLines;
    private TableLayout tableInventoryLines;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myActivity = this;

        findViewById();

        btnSelectDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BluetoothSettings.SetScaner(myActivity);
            }
        });
        btnConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BluetoothConnect.Connect();
            }
        });
        btnDisconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BluetoothConnect.Stop(myActivity);
            }
        });

        // Bind Bluetooth Service(Must bind service before start)
        BluetoothConnect.BindService(myActivity);

        GetData();

        new Thread() {

            @Override
            public void run() {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                BluetoothConnect.Connect();
            }

        }.start();

        // Temp code 1
        /*new Thread(new Runnable() {
            @Override
            public void run() {
                // DO your work here
                // use this to start and trigger a service
                Intent i= new Intent(getApplicationContext(), MyService.class);
                // potentially add data to the intent
                i.putExtra("KEY1", "Value to be used by the service");
                myActivity.startService(i);
            }
        }).start();*/

        // Temp code 2
        /*new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Intent i= new Intent(getApplicationContext(), MyService.class);
                // potentially add data to the intent
                i.putExtra("KEY1", "Value to be used by the service");
                myActivity.startService(i);
            }
        });*/

        // use this to start and trigger a service
        /*Intent i= new Intent(getApplicationContext(), MyService.class);
        // potentially add data to the intent
        i.putExtra("KEY1", "Value to be used by the service");
        this.startService(i);*/


        // Set database configuration
        // Local PC/LENOVO-PC
        /*mMSSQLDatabase.setMSSQLSettings(
                "192.168.1.2",
                "57654",
                "SQLEXPRESS",
                "auto",
                "sa",
                "mssql"
        );*/
        // Automatiq online/outside
        /*mMSSQLDatabase.setMSSQLSettings(
                "xxx.xxx.xxx.xxx",
                "xxxx",
                "",
                "database",
                "username",
                "password"
        );*/
        // Automatiq LAN/inside
        /*mMSSQLDatabase.setMSSQLSettings(
                "xxx.xxx.xxx.xxx",
                "xxxx",
                "",
                "database",
                "username",
                "password"
        );*/

        // Get database connection string
//        mMSSQLConnectionString = mMSSQLDatabase.getMSSQLConnectionString();
    }

    public class MyService extends Service {
        @Override
        public int onStartCommand(Intent intent, int flags, int startId) {
            //TODO do something useful
            // Bind Bluetooth Service(Must bind service before start)
            BluetoothConnect.BindService(myActivity);

            GetData();

            new Thread() {

                @Override
                public void run() {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    BluetoothConnect.Connect();

                    // setBroadcast();
                }

            }.start();

            return Service.START_STICKY;
        }

        @Nullable
        @Override
        public IBinder onBind(Intent intent) {
            return null;
        }
    }

    private void findViewById() {
        btnSelectDevice = (Button) findViewById(R.id.btnSelectDevice);
        btnConnect = (Button) findViewById(R.id.btnConnect);
        btnDisconnect = (Button) findViewById(R.id.btnDisconnect);
        tvGetData = (TextView) findViewById(R.id.tvGetData);
        scrollViewInventoryLines = (ScrollView) findViewById(R.id.scrollViewInventoryLines);
        tableInventoryLines = (TableLayout) findViewById(R.id.tableInventoryLines);
    }

    private void setBroadcast() {
        // Setting up data broadcasting
        mReadBroadcast = new ReadBroadcast();
        IntentFilter filter = new IntentFilter();
        filter.addAction(SendConstant.GetDataAction);
        filter.addAction(SendConstant.GetReadDataAction);
        filter.addAction(SendConstant.GetBatteryDataAction);
        registerReceiver(mReadBroadcast, filter);

    }

    public class ReadBroadcast extends BroadcastReceiver {

        public ReadBroadcast() {

        }

        public void getResultData(String data) {
            // generateProductListTable(getProductListJSON(data));

            // Log.d("onReceive", data);
        }

        String finalData = "";

        @Override
        public void onReceive(Context context, Intent intent) {
            // Accept electricity data broadcast
            /*if (intent.getAction().equals(SendConstant.GetBatteryDataAction)) {

                String data = intent.getStringExtra(SendConstant.GetBatteryData);

                ((EditText) findViewById(R.id.editText1)).append(data);
            }*/


            String data = "";

            // Receiving broadcast of data
            if (intent.getAction().equals(SendConstant.GetDataAction)) {
                data = intent.getStringExtra(SendConstant.GetData);
                // data = intent.getDataString();

                if(!data.equals("\r")) {
                    finalData += data;
                } else {
                    // ((TextView) findViewById(R.id.tvGetData)).append(data);
                    // ((TextView) findViewById(R.id.tvGetData)).setText("Scan Data: "+finalData);

                    // Temp code 1
                    /*new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            tvGetData.setText("Scan Data: "+finalData);
                        }
                    });*/

                    // To make this execute even the app is not in focus
                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            tvGetData.setText("Scan Data: "+finalData);
                        }
                    });

                    // tvGetData.setText("Scan Data: "+finalData);

                    Log.d("onReceive", finalData);

                    generateProductListTable(getProductListJSON(finalData));

                    finalData = "";
                }


                //if(data.equals(" ")) {
                /*StringBuilder sb = new StringBuilder();
                for (char c : finalData.toCharArray())
                    sb.append((int)c);

                Integer mInt = Integer.valueOf(sb.toString());
                System.out.println(mInt);

                Log.d("onReceive", Integer.toString(mInt));*/

                //int ascii = data.charAt(0);// Good
                // int ascii = finalData.charAt(12);
                    //Log.d("onReceive", Integer.toString(ascii)); // Good


                //i++;
            }

//            if (!data.trim().isEmpty()) {
//            if(data.equals("\r\n")) {
//            if(data.equals("\n")) {
            /*if(data.equals("\n\r")) {
                Log.d("onReceive", finalData);
            }*/
            // Log.d("onReceive", finalData);
//            Log.d("onReceive", Integer.toString(i));
            //Log.d("onReceive", finalData);
            // generateProductListTable(getProductListJSON(finalData));

            /*final String finalData2 = finalData;
            new Thread() {

                @Override
                public void run() {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    generateProductListTable(getProductListJSON(finalData2));
                }

            }.start();*/

            // Receive broadcasts that send data
            /*if (intent.getAction().equals(SendConstant.GetReadDataAction)) {
                String name = intent.getStringExtra(SendConstant.GetReadName);
                String data = intent.getStringExtra(SendConstant.GetReadData);

                // If you receive the type of charge
                if (name.equals(myActivity.getString(R.string.gs_read_charge))) {
                    // Get 0, 1 mark
                    data = data.substring(7, 8);
                    if (data.equals("0")) {
                        data = myActivity
                                .getString(R.string.gs_usb_charge_fast);

                    } else {
                        data = myActivity
                                .getString(R.string.gs_usb_charge_normal);

                    }
                    ((EditText) findViewById(R.id.editText1)).append(name + ":"
                            + data);
                } else {
                    ((EditText) findViewById(R.id.editText1)).append(name + ":"
                            + data);
                }
            }*/
        }

    }

    /**
     * Setting data acceptance/ Settings Data
     */
    private void GetData() {

        /**
         * Connection succeeded
         */
        BluetoothConnect.SetOnConnectedListener(new OnConnectedListener() {

            @Override
            public void Connected() {
                Toast.makeText(myActivity, "Connection succeeded.", Toast.LENGTH_SHORT).show();
            }

        });
        /**
         * Disconnect
         */
        BluetoothConnect.SetOnDisconnectListener(new OnDisconnectListener() {

            @Override
            public void Disconnected() {
                Toast.makeText(myActivity, "Disconnected.", Toast.LENGTH_SHORT).show();
            }

        });
    }

    @Override
    protected void onStart() {
        // Set the broadcast to read data
        setBroadcast();
        super.onStart();
    }

    /*@Override
    protected void onStop() {
        if (mReadBroadcast != null) {
            // Cancel broadcast
            this.unregisterReceiver(mReadBroadcast);
        }
        super.onStop();
    }*/

    @Override
    protected void onDestroy() {
        BluetoothConnect.UnBindService(myActivity);
        super.onDestroy();
    }

    private JSONArray getProductListJSON(String IMUPCN) {
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);

            Class.forName("net.sourceforge.jtds.jdbc.Driver");
            Connection conn = DriverManager.getConnection(mMSSQLConnectionString);

//            String companyQuery = company != "- All -" ? "AND i.COMPANY = '" + company + "' ": "";
//            String companyJSON = company == "- All -" ? "": company;

            // Inventory lines
            String query = "SELECT TOP 5 " +
                    "IMLITM" +
                    ",IMDSC1 " +
                    "FROM auto..tblProductList " +
                    "WHERE IMUPCN = '" + IMUPCN + "'" +
                    "";
            //String query = "EXEC Select5Products";

            Log.d("query", query);

            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);

            JSONArray jsonArrayInventoryLines = null;
            // JSONObject jsonObjInventoryLines = null;
            JSONObject jsonObjInventoryLines;

            //if(rs.isAfterLast()) {
            //if(rs.next()) {
            if (rs.isBeforeFirst() ) {
                jsonArrayInventoryLines = new JSONArray();
                //rs.beforeFirst();
                //Log.d("generate", "found null");
                //return null;
            }

            //jsonArrayInventoryLines = new JSONArray();

            while(rs.next()) {
                try {
                    //jsonArrayInventoryLines = new JSONArray();
                    jsonObjInventoryLines = new JSONObject();

                    // Inventory lines
//                    jsonObjInventoryLines.put("IMLITM", Integer.toString(rs.getInt("INTERNAL_LOCATION_INV")));
                    jsonObjInventoryLines.put("IMLITM", rs.getString("IMLITM"));
                    jsonObjInventoryLines.put("IMDSC1", rs.getString("IMDSC1"));

                    jsonArrayInventoryLines.put(jsonObjInventoryLines);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                // Toast.makeText(this, Integer.toString(rs.getInt("INTERNAL_LOCATION_INV")), Toast.LENGTH_SHORT).show();
            }

            // Existing inventory + counted lines JSON, comment after use
            if (jsonArrayInventoryLines != null) {
                // Log.d("getInventoryLinesJSON", jsonArrayInventoryLines.toString(2));
                Log.d("getInventoryLinesJSON", jsonArrayInventoryLines.toString(0));
            }

            stmt.close();
            conn.close();

            Log.d("getInv", String.valueOf(jsonArrayInventoryLines));

            return jsonArrayInventoryLines;

        } catch (SQLException se) {
            Log.e("ERROR", se.getMessage());
            Log.e("ERROR", "A");
        } catch (ClassNotFoundException e) {
            Log.e("ERROR", e.getMessage());
            Log.e("ERROR", "B");
        } catch (Exception e) {
            Log.e("ERROR", e.getMessage());
            Log.e("ERROR", "C");
        }

        return null;
    }

    private void generateProductListTable(JSONArray jsonArrayInventoryLines) {
        JSONObject jsonObjectProductList = null;
        Log.d("generate", String.valueOf(jsonArrayInventoryLines));
        // Clear the table
        tableInventoryLines.removeAllViews();

        tableInventoryLines.setBackgroundColor(Color.WHITE);

        //if (jsonArrayInventoryLines.equals(null)) {
        if (jsonArrayInventoryLines == null) {
        //if (String.valueOf(jsonArrayInventoryLines) == "[]") {
        //if(String.valueOf(jsonArrayInventoryLines) != null && String.valueOf(jsonArrayInventoryLines).length() > 0) {
            Toast.makeText(this, "No item(s) found.", Toast.LENGTH_SHORT).show();

            return;
        }
        else {
            Toast.makeText(this, "Item(s) found.", Toast.LENGTH_SHORT).show();
        }

        // Row 1
        TableRow tbrow = new TableRow(this);

        // Column 1
        TextView tvWarehouse = new TextView(this);
        tvWarehouse.setTextSize(18);
        tvWarehouse.setText("ID");
        tvWarehouse.setTextColor(Color.BLACK);
        tvWarehouse.setBackgroundColor(Color.GRAY);
        // tvWarehouse.setBackgroundResource(R.drawable.cell_shape_gray_background);
        tvWarehouse.setPadding(10,10,0,10);
        tvWarehouse.setGravity(Gravity.CENTER);
        // Column 1

        tbrow.addView(tvWarehouse);

        // Column 2
        TextView tvIMDSC1 = new TextView(this);
        tvIMDSC1.setTextSize(18);
        tvIMDSC1.setText("DESCRIPTION");
        tvIMDSC1.setTextColor(Color.BLACK);
        tvIMDSC1.setBackgroundColor(Color.GRAY);
        // tvIMDSC1.setBackgroundResource(R.drawable.cell_shape_gray_background);
        tvIMDSC1.setPadding(10,10,0,10);
        tvIMDSC1.setGravity(Gravity.CENTER);
        // Column 2

        tbrow.addView(tvIMDSC1);

        tableInventoryLines.addView(tbrow);

        try {
            for(int i=0; i < jsonArrayInventoryLines.length(); i++) {
                jsonObjectProductList = jsonArrayInventoryLines.getJSONObject(i);

                // Row 2
                TableRow tbrow2 = new TableRow(this);

                // Column 1
                final TextView tvIMLITMValue = new TextView(this);
                tvIMLITMValue.setTextSize(18);

                tvIMLITMValue.setText(jsonObjectProductList.optString("IMLITM"));
                // tvIMLITMValue.setText("COLUMN1");

                // TODO: Changing WHITE to BLACK
                tvIMLITMValue.setTextColor(Color.BLACK);
                // tvIMLITMValue.setBackgroundResource(R.drawable.cell_shape);
                tvIMLITMValue.setPadding(10, 10, 0, 10);
                tvIMLITMValue.setGravity(Gravity.START);
                // Column 1

                tbrow2.addView(tvIMLITMValue);

                // Column 2
                final TextView tvIMDSC1Value = new TextView(this);
                tvIMDSC1Value.setTextSize(18);

                tvIMDSC1Value.setText(jsonObjectProductList.optString("IMDSC1"));
                // tvIMDSC1Value.setText("COLUMN2");

                // TODO: Changing WHITE to BLACK
                tvIMDSC1Value.setTextColor(Color.BLACK);
                // tvWarehouseValue.setBackgroundResource(R.drawable.cell_shape);
                tvIMDSC1Value.setPadding(10, 10, 0, 10);
                tvIMDSC1Value.setGravity(Gravity.START);
                // Column 2

                tbrow2.addView(tvIMDSC1Value);

                tableInventoryLines.addView(tbrow2);
                // Row 1

            }
        } catch (JSONException e) {e.printStackTrace();}
    }
}
